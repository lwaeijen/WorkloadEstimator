#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/CommandLine.h"
#include "llvm-c/Core.h"
#include <iostream>
#include <string>
using namespace llvm;
using namespace cl;
using namespace std;

#define MAIN "main"

// elminate_overhead default is false;
static cl::opt<bool> eliminateOverhead ("eli", cl::desc("Eliminate the overhead caused by for.inc and for.con"));
static cl::opt<bool> writeExcel ("w2e", cl::desc("Write results to excel file"));
static cl::opt<string> functionInMain ("fNameInMain", cl::desc("Benchmark function called in Main"));
static cl::opt<string> functionNotRecursive ("fNameNotR", cl::desc("Benchmark function not recursive, e.g. FFT"));


namespace {
  struct DynCount : public ModulePass {
    static char ID;
    DynCount() : ModulePass(ID) {}
    bool enterFunction = false;
    bool runOnModule(Module &M);
    bool runOnFunction(Function &F);
    bool InsertBasicInstCall(BasicBlock &B, Function &F, Constant *logFunc, Constant *printFunc, Constant *write2Excel);
    bool InsertBenchmarkCall(Function::iterator blk, Function &F, Constant *startBench, Constant *stopBench);
    int getTypeString(Type *type);
  };
}


bool DynCount::runOnModule(Module &M){
	for (Module::iterator fct = M.begin(), fct_end = M.end(); fct != fct_end; ++fct){
		Function &F = *fct;
		runOnFunction(F);
	}
  return false;
}


bool DynCount::runOnFunction(Function &F) {
  LLVMContext &Ctx = F.getContext();
  Module &M = *F.getParent();
  std::vector<Type*> paramTypes = {Type::getInt32Ty(Ctx),Type::getInt32Ty(Ctx)};
  Type *retType = Type::getVoidTy(Ctx);
  FunctionType *logFuncType = FunctionType::get(retType, paramTypes, false);
  FunctionType *printFuncType = FunctionType::get(retType, false);

  Constant *logFunc = M.getOrInsertFunction("logop", logFuncType);
  Constant *printFunc = M.getOrInsertFunction("printop", printFuncType);
  Constant *startBench = M.getOrInsertFunction("startBench", printFuncType);
  Constant *stopBench = M.getOrInsertFunction("stopBench", printFuncType);
  Constant *write2Excel = M.getOrInsertFunction("write2Excel", printFuncType);

  for (Function::iterator blk = F.begin(), blk_end = F.end(); blk != blk_end; ++blk) {
    BasicBlock &B = *blk;
    StringRef Fname = F.getName();
    StringRef BBname = B.getName();

    // Eliminate two types of blocks that cause overhead.
    if (!BBname.find("for.cond") || !BBname.find("for.inc")){   //string.find("xx") if find, then return 0.....
      if (!eliminateOverhead) InsertBasicInstCall(B, F, logFunc, printFunc, write2Excel);
      }
    else{
    	InsertBasicInstCall(B, F, logFunc, printFunc, write2Excel);
    }
    InsertBenchmarkCall(blk, F, startBench, stopBench);
  }

  return false;
}


bool DynCount::InsertBenchmarkCall(Function::iterator blk, Function &F, Constant *startBench, Constant *stopBench){
  StringRef Fname = F.getName();
  StringRef BBname = F.getName();
  BasicBlock &B = *blk;
  IRBuilder<> builder(&B);

  for (BasicBlock::iterator ist = B.begin(), end = B.end(); ist != end; ++ist){
    Instruction &I = *ist;

    if (!functionInMain.empty()){
      /*
        main(){
          startBench();
          call MeasuredFunctionName;
          stopBench();
        }
      */
      if(Fname == MAIN){
       if (isa<CallInst>(I)) {
         StringRef Callname = cast<CallInst>(I).getCalledFunction()->getName();
         if (Callname == functionInMain){
           builder.SetInsertPoint(&I);
           builder.CreateCall(startBench);
           BasicBlock::iterator it = ist;
           ++it;
           builder.SetInsertPoint(&*it);
           builder.CreateCall(stopBench);
         }
        }
      }
    }
    else{ // Measure any function that is not recursive
      /*
          MeasuredFunctionName(){
            startBench();
            .....
            stopBench();
            ret
          }
      */
      if(Fname == functionNotRecursive){
        if(blk == F.begin() && ist == B.begin()){
          builder.SetInsertPoint(&I);
          builder.CreateCall(startBench);
        }
        else if(I.getOpcode()==1){
          builder.SetInsertPoint(&I);
          builder.CreateCall(stopBench);
        }
      }
    }
  }
  return false;
}


bool DynCount::InsertBasicInstCall(BasicBlock &B, Function &F, Constant *logFunc,
  Constant *printFunc, Constant *write2Excel){
  int phiCount = 0;

  for (BasicBlock::iterator ist = B.begin(), end = B.end(); ist != end; ++ist) {
    Instruction &I = *ist;

    StringRef Fname = F.getName();
    int opcode = I.getOpcode();
    Type *Itype = I.getType();

    //I.dump();
    Value *Vopcode, *VdataType, *Vopcodee, *VdataTypee;
    IRBuilder<> builder(&B);

    // phi node mush be the first instr of one BB
    if (isa<PHINode>(&I)){
    	phiCount++;
    }else{
      builder.SetInsertPoint(&I);
    }

    VdataType = ConstantInt::get(Type::getInt32Ty(F.getContext()), 0);
    Vopcode = ConstantInt::get(Type::getInt32Ty(F.getContext()), opcode);

    if(isa<llvm::BinaryOperator>(&I)){

    	/*  if detected data type is vector, then construct function call for #(numbElements) times with element type.
    	    namely splitting vector operations, for instance

				  call void @logop(i32 12, i32 3)
  				call void @logop(i32 12, i32 3)
  				call void @logop(i32 12, i32 14)
  				%121 = fadd <2 x double> %wide.load435, %wide.load439
    	*/

    	if(Itype->isVectorTy()){
    		//I.dump();
    		VectorType *vecType=static_cast<VectorType*>(Itype);
    		int numbElements = vecType->getNumElements();
    		VdataType = ConstantInt::get(Type::getInt32Ty(F.getContext()), getTypeString(vecType->getElementType()));
    		Value* argss[] = {Vopcode, VdataType};
		 		for(int i=0; i<numbElements; i++){
		 			builder.CreateCall(logFunc, argss);
		 		}
    	}

      VdataType = ConstantInt::get(Type::getInt32Ty(F.getContext()), getTypeString(Itype) );
    }

    Value* args[] = {Vopcode, VdataType};

    // in case of several phi nodes
    if(ist == B.getFirstNonPHI()->getIterator() && phiCount){
    	Vopcodee = ConstantInt::get(Type::getInt32Ty(F.getContext()), 53);  //opcode of phi nodes
      VdataTypee = ConstantInt::get(Type::getInt32Ty(F.getContext()), 0);
    	Value* argss[] = {Vopcodee, VdataTypee};
    	for(int i = 0; i<phiCount; i++){
    		builder.CreateCall(logFunc, argss);
    	}
    	phiCount = 0;
    }

    if(!isa<PHINode>(&I)) builder.CreateCall(logFunc, args); // including the ret after call stoping benchmark.

    // Check if it is return instruction
    if(opcode==1){
      IRBuilder<> builder(&B);
      builder.SetInsertPoint(&I);
      if(Fname == MAIN){
        if (writeExcel){
          builder.CreateCall(write2Excel);
        }else{
          builder.CreateCall(printFunc);
        }
    	}
    }
  }
  return false;
}


int DynCount::getTypeString(Type *type) {
    /*
        DataType:
        1: half; 2: float; 3: double; 4: 80-bit floating point
        5: 128-bit floating point type (112-bit mantissa)
        6: 128-bit floating point type (two 64-bits, PowerPC)
        7: int1; 8: int8; 9: int16; 10: int32 11: int64; 12: int128;
        13: Pointers; 14: Vector; 15: Array
    */

  switch(type->getTypeID()) {
    case Type::IntegerTyID:{
      IntegerType *intType=static_cast<IntegerType*>(type);
      int bitWidth = intType->getBitWidth();
      switch(bitWidth) {
      	case 1: return 7;		// int1
        case 8: return 8;		// int8
        case 16: return 9;		// int16
        case 32: return 10;		// int32
        case 64: return 11;		// int64
        case 128: return 12;	// int128
      }
    }
    case Type::HalfTyID: return 1;		// half
    case Type::FloatTyID: return 2;		// float
    case Type::DoubleTyID: return 3;	// double
    case Type::X86_FP80TyID: return 4;	// 80-bit floating point
    case Type::FP128TyID: return 5;		// 128-bit floating point type (112-bit mantissa)
    case Type::PPC_FP128TyID: return 6; // 128-bit floating point type (two 64-bits, PowerPC)
    case Type::PointerTyID: return 13;	// pointer
    case Type::VectorTyID: return 14; 	// vector
    case Type::ArrayTyID: return 15;    // array
    default: return 0;
  }
}

char DynCount::ID = 0;

// Automatically enable the pass.
// http://adriansampson.net/blog/clangpass.html
static RegisterPass<DynCount> X("dyncount", "Dynamic intruction count",
                             false /* Only looks at CFG */,
                             false /* Analysis Pass */);

static void registerDynCount(const PassManagerBuilder &,
                         legacy::PassManagerBase &PM) {
  PM.add(new DynCount());
}
static RegisterStandardPasses
  RegisterMyPass(PassManagerBuilder::EP_EarlyAsPossible,
                 registerDynCount);
