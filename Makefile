##################################
# Adjust paths below as desired
#
BUILD_DIR=build

#use the PolyBencACC submodule
BENCHMARK_DIR=PolyBench-ACC/OpenACC/

#List the sources of the individual benchmarks (. for all c files, alternatively overwrite BENCHMARK_SRCS with you own list)
BENCHMARK_SUB_DIRS=stencils linear-algebra datamining
#exclude convolution because the function names do not match the file names
EXCLUDE=convolution*
BENCHMARK_SRCS = $(foreach D, $(BENCHMARK_SUB_DIRS), $(shell find $(join $(BENCHMARK_DIR), $(D)) -name "*.c" $(foreach e, $(EXCLUDE), ! -name "$(e)")))

#generic files required to be linked with each benchmark
SUPPORT_FILES=PolyBench-ACC/common/polybench.c

##################################
# Pass Settings
#
ELIMINATE_OVERHEAD ?= true
W2E ?= false

##################################
# Tool Settings
#
CC=$(shell which clang-5.0)
CXX=$(shell which clang++-5.0)
CLANG=$(shell which clang-5.0)
LLC=$(shell which llc-5.0)
OPT=$(shell which opt-5.0)
CFLAGS = -O3
LIBS= -lm

##################################
# Do not edit below this line!!
#
PASS_BUILD_DIR=$(BUILD_DIR)/Dyncount/
PASS_MAKEFILE=$(PASS_BUILD_DIR)/Makefile
PASS_SO=$(PASS_BUILD_DIR)/DynCount/libDynCountPass.so

#sources of support files
SUPPORT_BUILD_DIR=$(BUILD_DIR)/support_files/
SUPPORT_FILES += DynCount/rtlib.c
SUPPORT_OBJS = $(foreach f, $(SUPPORT_FILES:.c=.o), $(join $(SUPPORT_BUILD_DIR), $(f)) )

#Collect benchmark sources
LOGS = $(BENCHMARK_SRCS:.c=.log)
GENERATED=$(BENCHMARK_SRCS:.c=.bc) $(BENCHMARK_SRCS:.c=.opt.bc) $(BENCHMARK_SRCS:.c=.opt.o) $(BENCHMARK_SRCS:.c=.exe) $(LOGS)

fummy:
	@for l in $(LOGS); do echo $$l; done

#build all logs
all:$(LOGS)

%.bc:%.c
	$(CLANG) $(CFLAGS) -c -emit-llvm -fno-inline-functions $(foreach f, $(SUPPORT_FILES), -I $(dir $(f)))  -I $(dir $@) $< -o $@

#NOTE: kernel name is derived from filename!
%.opt.bc:%.bc $(PASS_SO)
	$(OPT) -load $(PASS_SO) -dyncount -eli=$(ELIMINATE_OVERHEAD) -fNameNotR=kernel_$(subst -,_,$(basename $(notdir $<))) -w2e=$(W2E) $< -o $@

%.opt.o:%.opt.bc
	$(LLC) -filetype=obj $< -relocation-model=pic -o $@

%.exe:%.opt.o $(SUPPORT_OBJS)
	$(CC) $< $(SUPPORT_OBJS) -o $@ $(LIBS)

%.log:%.exe
	./$< | tee $@

#regular cc for support libs
$(SUPPORT_BUILD_DIR)%.o:%.c
	mkdir -p $(dir $@)
	$(CC) -c $< -o $@

#make pass shared object
build:$(PASS_SO)
$(PASS_SO):$(PASS_MAKEFILE)
	$(MAKE) -C $(PASS_BUILD_DIR)

#generate the makefile using cmake
$(PASS_MAKEFILE):$(PASS_BUILD_DIR)
	cd $< && CXX=$(CXX) CC=$(CC) cmake $(abspath $(CURDIR))

#create build directory if it doesn't exist yet
$(PASS_BUILD_DIR):
	mkdir -p $@

#clean files (generated intermediate files are added to make sure everything is cleaned, although typically make should remove intermediate files)
clean:
	rm -rf $(BUILD_DIR) $(LOGS) $(GENERATED)

#Do not rebuild the pass libs and support objects all the time
.PRECIOUS:$(PASS_SO) $(SUPPORT_OBJS)
