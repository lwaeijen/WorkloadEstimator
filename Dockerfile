FROM debian:stretch-backports
ARG DEBIAN_FRONTEND="noninteractive"

RUN apt update && \
    apt install -y --no-install-recommends \
      git \
      make \
      cmake \
      llvm-5.0 \
      llvm-5.0-dev \
      clang-5.0 \
      clang++-5.0
