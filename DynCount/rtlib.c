#include <stdio.h>
#define DataTypeNumb 16
#define BitOperationNumb 19

unsigned long long total_ops = 0;
unsigned long long bitbinary_ops = 0;
unsigned long long Ops[65];
unsigned long long Binary_ops[BitOperationNumb][DataTypeNumb];
int start_Bench = 0;
char *mapCodeToName(int Op);
char *mapDataCodeToName(int DataType);
void printop();
void logBinaryOp(int Op, int DataType);
void stopBench();
void startBench();
void write2Excel();

void logop(int Op, int DataType) {

    if(Op < 65){
        if(start_Bench ==1){
            Ops[Op]++;
            if (DataType != 0){
                Binary_ops[Op-10][DataType]++;
            }
        }
    }

}

void startBench(){
	start_Bench = 1;
	printf("Start bench! \n");
}

void stopBench(){
	start_Bench = 0;
	printf("Stop bench! \n");
}

void printop(){
    int i,j;
    printf("----------------------------------------\n");
    printf("       Dynamic Instruction Count        \n");
    printf("----------------------------------------\n");
    for(i=1; i<65; i++){
        if(Ops[i]!=0){
            printf("%-16s =      %-8llu\n", mapCodeToName(i), Ops[i]);
			total_ops = total_ops + Ops[i];
            if (i>=11 && i<=28) bitbinary_ops = bitbinary_ops + Ops[i];
        }
    }
    printf("----------------------------------------\n");
    printf("Total number     =      %-8llu\n",total_ops);
    printf("----------------------------------------\n");
    printf("         Binary Operation Count         \n");
    printf("----------------------------------------\n");
    for(i=1; i<BitOperationNumb; i++){
        for (j=1; j<DataTypeNumb; j++){
            if (Binary_ops[i][j]!=0)
                printf("%-5s %-10s =      %-8llu\n", mapCodeToName(i+10), mapDataCodeToName(j), Binary_ops[i][j]);
        }
    }
    printf("----------------------------------------\n");
    printf("    (Bitwise) Binary Operation Count    \n");
    printf("----------------------------------------\n");
    printf("Total number     =      %-8llu\n",bitbinary_ops);
    printf("----------------------------------------\n");
}

void write2Excel(){
    int i,j;
    printf(">>\n");
    for(i=1; i<65; i++){
        if(Ops[i]!=0){
            printf("%s,%llu\n",mapCodeToName(i), Ops[i]) ;
            total_ops = total_ops + Ops[i];
            if (i>=11 && i<=28) bitbinary_ops = bitbinary_ops + Ops[i];
        }
    }

    for(i=1; i<BitOperationNumb; i++){
        for (j=1; j<DataTypeNumb; j++){
            if (Binary_ops[i][j]!=0)
                printf("%s_%s,%llu\n", mapCodeToName(i+10), mapDataCodeToName(j), Binary_ops[i][j]);
        }
    }
    printf("<<\n");
}

char *mapDataCodeToName(int DataType) {
    /*
        DataType:
        1: half; 2: float; 3: double; 4: 80-bit floating point
        5: 128-bit floating point type (112-bit mantissa)
        6: 128-bit floating point type (two 64-bits, PowerPC)
        7: int1; 8: int8; 9: int16; 10: int32 11: int64; 12: int128;
        13: Pointers; 14: Vector; 15: Array
    */

    switch (DataType){
        case 1: return "half";
        case 2: return "float";
        case 3: return "double";
        case 4: return "float80";
        case 5: return "float128m";
        case 6: return "float128p";
        case 7: return "int1";
        case 8: return "int8";
        case 9: return "int16";
        case 10: return "int32";
        case 11: return "int64";
        case 12: return "int128";
        case 13: return "pointer";
        case 14: return "vector";
        case 15: return "array";
        default: return "unknown";
    }
}

char *mapCodeToName(int Op) {

    if (Op == 1) return "ret";
    if (Op == 2) return "br";
    if (Op == 3) return "switch";
    if (Op == 4) return "indirectbr";
    if (Op == 5) return "invoke";
    if (Op == 6) return "resume";
    if (Op == 7) return "unreachable";
    if (Op == 8) return "cleanupret";
    if (Op == 9) return "catchret";
    if (Op == 10) return "catchswitch";
    if (Op == 11) return "add";
    if (Op == 12) return "fadd";
    if (Op == 13) return "sub";
    if (Op == 14) return "fsub";
    if (Op == 15) return "mul";
    if (Op == 16) return "fmul";
    if (Op == 17) return "udiv";
    if (Op == 18) return "sdiv";
    if (Op == 19) return "fdiv";
    if (Op == 20) return "urem";
    if (Op == 21) return "srem";
    if (Op == 22) return "frem";
    if (Op == 23) return "shl";
    if (Op == 24) return "lshr";
    if (Op == 25) return "ashr";
    if (Op == 26) return "and";
    if (Op == 27) return "or";
    if (Op == 28) return "xor";
    if (Op == 29) return "alloca";
    if (Op == 30) return "load";
    if (Op == 31) return "store";
    if (Op == 32) return "getelementptr";
    if (Op == 33) return "fence";
    if (Op == 34) return "cmpxchg";
    if (Op == 35) return "atomicrmw";
    if (Op == 36) return "trunc";
    if (Op == 37) return "zext";
    if (Op == 38) return "sext";
    if (Op == 39) return "fptoui";
    if (Op == 40) return "fptosi";
    if (Op == 41) return "uitofp";
    if (Op == 42) return "sitofp";
    if (Op == 43) return "fptrunc";
    if (Op == 44) return "fpext";
    if (Op == 45) return "ptrtoint";
    if (Op == 46) return "inttoptr";
    if (Op == 47) return "bitcast";
    if (Op == 48) return "addrspacecast";
    if (Op == 49) return "cleanuppad";
    if (Op == 50) return "catchpad";
    if (Op == 51) return "icmp";
    if (Op == 52) return "fcmp";
    if (Op == 53) return "phi";
    if (Op == 54) return "call";
    if (Op == 55) return "select";
    if (Op == 56) return "<Invalid operator>";
    if (Op == 57) return "<Invalid operator>";
    if (Op == 58) return "va_arg";
    if (Op == 59) return "extractelement";
    if (Op == 60) return "insertelement";
    if (Op == 61) return "shufflevector";
    if (Op == 62) return "extractvalue";
    if (Op == 63) return "insertvalue";
    if (Op == 64) return "landingpad";
    return "INVALID";
}
