# Workload Estimator

LLVM based tool to estimate intrinsic workload of applications, as defined in TODO: reference paper.

# Prerequisites
Your system will need the following tools installed to build the workload estimator and run the benchmarks
   * llvm-5.0
   * git
   * cmake >=3.1

On a debian system you can install these tools using:
```shell
sudo apt install git cmake llvm-5.0
```
Note: on the current stable version of debian llvm-5.0 is not available from the main package repositories.
For this debian and older please add [backports](https://backports.debian.org/) and install the llvm-5.0 package as suggested above.

Alternatively the provided Dockerfile can be used to build a docker container that will be able to build and execute this workload estimator.

# Run Benchmarks
The benchmarks are taken from [PolyBench/ACC](https://github.com/cavazos-lab/PolyBench-ACC).
To compile the workload estimator, which is implemented as a pass of llvm, and run these benchmarks simply execute:
```shell
make
```

# License
This work is licensed under the MIT license.
We ask however that if you use this work or a derivative thereof in your own publication to please cite the accompanying paper [TODO].


Repository iconn made by [monik](https://www.flaticon.com/authors/monkik) from [www.flaticon.com](https://www.flaticon.com/).
